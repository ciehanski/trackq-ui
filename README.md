# TrackQ UI

This repo contains the frontend code for the TrackQ project. Written in JavaScript using the React Framework

[Backend API](https://github.com/projectunic0rn/trackq-api)

## Setting Up for Dev

-   Run `npm install`
-   Run `npm run dev` this will start a webpack-dev-server with hot-reloading and will watch for any changes

## Installing new npm packages

If the npm package will be used by the project we do the following

```
npm install react --save
```

If the npm package is just for development like webpack we run

```
npm install webpack --save-dev
```

## Standards

### Coding Standards

This project uses [Prettier](https://prettier.io/), [ES Lint](https://eslint.org/) and [EditorConfig](https://editorconfig.org/) for enforcing standards. We recommend you find an extension for your editor of choice and install it.

-   Imports should be ordered with npm packages at the top of the file and then relative files after with a space inbetween

```
import React from 'react';
import { NavLink } from 'react-router-dom';

import Navigation from './components/Navigation';
```

### Folder Structure

-   Everything to do with the application is stored inside `src/` inside here we have the root of the application `index.js`.
-   SCSS files should be stored within the folder they are meant for. Example:
    -   `./src/routes/App/_index.scss` is to be imported inside the `./src/routes/App/index.jsx`
    -   The only global SCSS files are stored inside `./src/constants/scss` which are for SCSS variables or global stuff like `h1 | h2 | h3` etc.
