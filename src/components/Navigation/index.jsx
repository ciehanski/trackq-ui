import React from 'react';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faThLarge, faUser, faCogs } from '@fortawesome/free-solid-svg-icons';

import './_index.scss';

const Navigation = props => {
    return (
        <nav className="navbar">
            <div className="navbar__menu">
                <NavLink to="/" className="navbar__link">
                    <FontAwesomeIcon icon={faHome} size="2x" className="navbar__icon" />
                </NavLink>
                <NavLink to="#" className="navbar__link">
                    <FontAwesomeIcon icon={faThLarge} size="2x" className="navbar__icon" />
                </NavLink>
                <NavLink to="#" className="navbar__link">
                    <FontAwesomeIcon icon={faUser} size="2x" className="navbar__icon" />
                </NavLink>
                <NavLink to="#" className="navbar__button">
                    <FontAwesomeIcon icon={faCogs} size="2x" className="navbar__icon" />
                </NavLink>
            </div>
        </nav>
    );
};

export default Navigation;
