import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import Navigation from 'components/Navigation';

import './styles.scss';

ReactDOM.render(
    <BrowserRouter>
        <main className="app">
            <Navigation />
        </main>
    </BrowserRouter>,
    document.getElementById('root')
);
