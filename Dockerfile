FROM node:8
RUN mkdir /usr/src/trackq-frontend
WORKDIR /usr/src/trackq-frontend
COPY package.json ./
ENV PATH=$PATH:/usr/src/trackq-frontend/node_modules/.bin
RUN npm install
COPY . ./
EXPOSE 3000
CMD ["npm", "start"]
