/* Path & Webpack */
const path = require('path');
const webpack = require('webpack');

/* Webpack Plugins */
const merge = require('webpack-merge');

/* Config */
const config = require('./webpack.config');

module.exports = merge(config, {
    devtool: 'inline-source-map',
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
    ],
    devServer: {
        contentBase: path.join(__dirname, '../public/'),
        publicPath: '/assets/',
        port: 3000,
        hot: true,
        inline: true,
        historyApiFallback: true,
    },
});
