/* Path & Webpack */
const path = require('path');

/* Webpack Plugins */
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

/* Build Process */
const build = path.resolve(__dirname, '../src');
const devMode = process.env.NODE_ENV !== 'production';

/* Plugins */
const flexbugs = require('postcss-flexbugs-fixes');

/* Assets */
const assets = path.resolve(__dirname, '../public/');

module.exports = {
    mode: devMode ? 'development' : 'production',
    context: `${build}/js/`,
    entry: {
        script: `${build}/index`,
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/styles.css',
        }),
    ],
    output: {
        path: assets,
        filename: 'js/[name].bundle.js',
    },
    resolve: {
        modules: ['node_modules', `${build}/js`],
        extensions: ['.js', '.jsx', '.css', '.scss'],
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /\.(css|scss)$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: 'inline',
                            plugins: () => flexbugs,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
            },
        ],
    },
};
